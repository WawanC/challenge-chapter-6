"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class user_game_histories extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      user_game_histories.belongsTo(models.user_game, {
        foreignKey: 'user_id',
        onDelete: 'CASCADE'
      })
    }
  }
  user_game_histories.init(
    {
      user_id: DataTypes.INTEGER,
      title: { type: DataTypes.STRING, allowNull: false },
      publisher: { type: DataTypes.STRING, allowNull: false },
      score: DataTypes.INTEGER,
      hours_time: DataTypes.INTEGER,
    },
    {
      sequelize,
      modelName: "user_game_histories",
    }
  );
  return user_game_histories;
};
