const router = require("express").Router();
const webController = require("../controllers/web");

router.get("/", webController.getHome);
router.get("/users", webController.getUsers);

router.post("/delete-game/:id", webController.deleteGame);

router.get("/add-game", webController.getAddGame);
router.post("/add-game", webController.postAddGame);

router.get("/edit-game/:id", webController.getEditGame);
router.post("/edit-game/:id", webController.postEditGame);

router.get("/register", webController.getRegister);
router.post("/register", webController.postRegister);

router.get("/login", webController.getLogin);
router.post("/login", webController.postLogin);

router.get("/edit-user/:userId", webController.getEditUser);
router.post("/edit-user/:userId", webController.postEditUser);
router.post("/delete-user/:userId", webController.postDeleteUserBio);

module.exports = router;
