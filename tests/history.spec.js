const request = require("supertest");
const app = require("../app");
const { faker } = require("@faker-js/faker");

const UserHistory = require("../models").user_game_histories;
const User = require("../models").user_game;

const username = faker.internet.userName();
const password = faker.internet.password();

describe("Get Histories Function", () => {
  test("200 Success", async () => {
    const response = await request(app).get("/api/game");
    const games = await UserHistory.findAll({
      include: [{ model: User }],
    });

    expect(response.statusCode).toBe(200);
    response.body.games.forEach((game, idx) => {
      for (const key in game) {
        expect(JSON.stringify(game[key])).toEqual(
          JSON.stringify(games[idx][key])
        );
      }
    });
  });
});

describe("Create History Function", () => {
  test("200 Success", async () => {
    const userResponse = await request(app).post(`/api/user`).send({
      username,
      password,
    });
    const userId = userResponse.body.user.id;

    const newHistory = {
      title: faker.word.noun(),
      publisher: faker.company.companyName(),
      score: +faker.random.numeric(2),
      hours_time: +faker.random.numeric(2),
    };

    const response = await request(app)
      .post(`/api/game/${userId}`)
      .send(newHistory);

    expect(response.statusCode).toBe(200);
    expect(response.body.game.title).toBe(newHistory.title);
    expect(response.body.game.publisher).toBe(newHistory.publisher);
    expect(response.body.game.score).toBe(newHistory.score);
    expect(response.body.game.hours_time).toBe(newHistory.hours_time);
  });

  test("422 Validation Failed", async () => {
    const newHistory = {
      title: faker.word.noun(),
      publisher: faker.company.companyName(),
      score: +faker.random.numeric(2),
      hours_time: +faker.random.numeric(2),
    };

    const response = await request(app).post(`/api/game/test`).send(newHistory);

    expect(response.statusCode).toBe(422);
  });
});

describe("Get History Function", () => {
  test("200 Success", async () => {
    const user = await User.findOne({ where: { username: username } });

    const games = await UserHistory.findAll({
      where: { user_id: user.id },
      raw: true,
    });

    const response = await request(app).get(`/api/game/${user.id}`);

    expect(response.statusCode).toBe(200);
    response.body.games.forEach((game, idx) => {
      expect(game.title).toBe(games[idx].title);
      expect(game.publisher).toBe(games[idx].publisher);
      expect(game.score).toBe(games[idx].score);
      expect(game.hours_time).toBe(games[idx].hours_time);
    });
  });

  test("422 Validation Failed", async () => {
    const response = await request(app).get(`/api/game/test`);

    expect(response.statusCode).toBe(422);
  });
});

describe("Update History Function", () => {
  test("200 Success", async () => {
    const user = await User.findOne({ where: { username: username } });

    const game = await UserHistory.findOne({ where: { user_id: user.id } });

    const updatedHistory = {
      title: faker.word.noun(),
      publisher: faker.company.companyName(),
      score: +faker.random.numeric(2),
      hours_time: +faker.random.numeric(2),
    };

    const response = await request(app)
      .patch(`/api/game/${game.id}`)
      .send(updatedHistory);

    const afterUpdateGame = await UserHistory.findOne({
      where: { id: game.id },
    });

    expect(response.statusCode).toBe(200);
    expect(response.body.beforeUpdate.title).toBe(game.title);
    expect(response.body.beforeUpdate.publisher).toBe(game.publisher);
    expect(response.body.beforeUpdate.score).toBe(game.score);
    expect(response.body.beforeUpdate.hours_time).toBe(game.hours_time);

    expect(response.body.afterUpdate.title).toBe(afterUpdateGame.title);
    expect(response.body.afterUpdate.publisher).toBe(afterUpdateGame.publisher);
    expect(response.body.afterUpdate.score).toBe(afterUpdateGame.score);
    expect(response.body.afterUpdate.hours_time).toBe(
      afterUpdateGame.hours_time
    );
  });

  test("404 Game Not Found", async () => {
    const updatedHistory = {
      title: faker.word.noun(),
      publisher: faker.company.companyName(),
      score: +faker.random.numeric(2),
      hours_time: +faker.random.numeric(2),
    };

    const response = await request(app)
      .patch(`/api/game/0`)
      .send(updatedHistory);

    expect(response.statusCode).toBe(404);
  });

  test("422 Validation Failed", async () => {
    const updatedHistory = {
      title: faker.word.noun(),
      publisher: faker.company.companyName(),
      score: +faker.random.numeric(2),
      hours_time: +faker.random.numeric(2),
    };

    const response = await request(app)
      .patch(`/api/game/test`)
      .send(updatedHistory);

    expect(response.statusCode).toBe(422);
  });
});

describe("Delete History Function", () => {
  test("200 Success", async () => {
    const user = await User.findOne({ where: { username: username } });

    const game = await UserHistory.findOne({ where: { user_id: user.id } });

    const response = await request(app).delete(`/api/game/${game.id}`);

    expect(response.statusCode).toBe(200);
    expect(response.body.beforeDeleted.title).toBe(game.title);
    expect(response.body.beforeDeleted.publisher).toBe(game.publisher);
    expect(response.body.beforeDeleted.score).toBe(game.score);
    expect(response.body.beforeDeleted.hours_time).toBe(game.hours_time);

    await User.destroy({ where: { username: username } });
  });

  test("404 Game Not Found", async () => {
    const response = await request(app).delete(`/api/game/0`);

    expect(response.statusCode).toBe(404);
  });

  test("422 Validation Failed", async () => {
    const response = await request(app).delete(`/api/game/test`);

    expect(response.statusCode).toBe(422);
  });
});
