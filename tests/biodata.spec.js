const request = require("supertest");
const app = require("../app");
const { faker } = require("@faker-js/faker");

const UserBiodata = require("../models").user_game_biodatas;
const User = require("../models").user_game;

const username = faker.internet.userName();
const password = faker.internet.password();

describe("Create User Biodata Function", () => {
  test("200 Success", async () => {
    const userResponse = await request(app).post(`/api/user`).send({
      username,
      password,
    });
    const userId = userResponse.body.user.id;

    const newBiodata = {
      first_name: faker.name.firstName(),
      last_name: faker.name.lastName(),
      country: faker.address.country(),
      email: faker.internet.email(),
    };

    const response = await request(app)
      .post(`/api/biodata/${userId}`)
      .send(newBiodata);

    expect(response.statusCode).toBe(200);
    expect(response.body.biodata.first_name).toEqual(newBiodata.first_name);
    expect(response.body.biodata.last_name).toEqual(newBiodata.last_name);
    expect(response.body.biodata.country).toEqual(newBiodata.country);
    expect(response.body.biodata.email).toEqual(newBiodata.email);
  });

  test("422 Validation Error", async () => {
    const newBiodata = {
      first_name: faker.name.firstName(),
      last_name: faker.name.lastName(),
      country: faker.address.country(),
      email: faker.internet.email(),
    };

    const response = await request(app)
      .post(`/api/biodata/test`)
      .send(newBiodata);

    expect(response.statusCode).toBe(422);
  });

  test("409 Already Exist", async () => {
    const userResponse = await request(app).post(`/api/user`).send({
      username,
      password,
    });
    const userId = userResponse.body.user.id;

    const newBiodata = {
      first_name: faker.name.firstName(),
      last_name: faker.name.lastName(),
      country: faker.address.country(),
      email: faker.internet.email(),
    };

    const createResponse = await request(app)
      .post(`/api/biodata/${userId}`)
      .send(newBiodata);

    const response = await request(app)
      .post(`/api/biodata/${userId}`)
      .send(newBiodata);

    expect(response.statusCode).toBe(409);
    expect(response.body.message).toBe("User biodata already exists");
  });

  test("404 User Not Found", async () => {
    const newBiodata = {
      first_name: faker.name.firstName(),
      last_name: faker.name.lastName(),
      country: faker.address.country(),
      email: faker.internet.email(),
    };

    const response = await request(app).post(`/api/biodata/0`).send(newBiodata);

    expect(response.statusCode).toBe(404);
    expect(response.body.message).toBe("User not found");
  });
});

describe("Get User Biodata Function", () => {
  test("200 Success", async () => {
    const user = await User.findOne({ where: { username: username } });

    const response = await request(app).get(`/api/biodata/${user.id}`);

    const userBiodata = await UserBiodata.findOne({
      where: { user_id: user.id },
      include: [{ model: User }],
    });

    expect(response.statusCode).toBe(200);
    expect(response.body.biodata.first_name).toEqual(userBiodata.first_name);
    expect(response.body.biodata.last_name).toEqual(userBiodata.last_name);
    expect(response.body.biodata.country).toEqual(userBiodata.country);
    expect(response.body.biodata.email).toEqual(userBiodata.email);
  });

  test("422 Validation Error", async () => {
    const response = await request(app).get(`/api/biodata/test`);

    expect(response.statusCode).toBe(422);
  });
});

describe("Update User Biodata Function", () => {
  test("200 Success", async () => {
    const user = await User.findOne({ where: { username: username } });

    const userBiodata = await UserBiodata.findOne({
      where: { user_id: user.id },
      include: [{ model: User }],
    });

    const newBiodata = {
      first_name: faker.name.firstName(),
      last_name: faker.name.lastName(),
      country: faker.address.country(),
      email: faker.internet.email(),
    };

    const response = await request(app)
      .patch(`/api/biodata/${user.id}`)
      .send(newBiodata);

    const userBiodataAfter = await UserBiodata.findOne({
      where: { user_id: user.id },
      include: [{ model: User }],
    });

    expect(response.statusCode).toBe(200);
    expect(response.body.beforeUpdate.first_name).toEqual(
      userBiodata.first_name
    );
    expect(response.body.beforeUpdate.last_name).toEqual(userBiodata.last_name);
    expect(response.body.beforeUpdate.country).toEqual(userBiodata.country);
    expect(response.body.beforeUpdate.email).toEqual(userBiodata.email);

    expect(response.body.afterUpdate.first_name).toEqual(
      userBiodataAfter.first_name
    );
    expect(response.body.afterUpdate.last_name).toEqual(
      userBiodataAfter.last_name
    );
    expect(response.body.afterUpdate.country).toEqual(userBiodataAfter.country);
    expect(response.body.afterUpdate.email).toEqual(userBiodataAfter.email);
  });

  test("422 Validation Error", async () => {
    const newBiodata = {
      first_name: faker.name.firstName(),
      last_name: faker.name.lastName(),
      country: faker.address.country(),
      email: faker.internet.email(),
    };

    const response = await request(app)
      .patch(`/api/biodata/test`)
      .send(newBiodata);

    expect(response.statusCode).toBe(422);
  });

  test("404 Biodata Not Found", async () => {
    const newBiodata = {
      first_name: faker.name.firstName(),
      last_name: faker.name.lastName(),
      country: faker.address.country(),
      email: faker.internet.email(),
    };

    const response = await request(app)
      .patch(`/api/biodata/0`)
      .send(newBiodata);

    expect(response.statusCode).toBe(404);
    expect(response.body.message).toBe("User biodata not found");
  });
});

describe("Delete User Biodata Function", () => {
  test("200 Success", async () => {
    const user = await User.findOne({ where: { username: username } });

    const userBiodata = await UserBiodata.findOne({
      where: { user_id: user.id },
      include: [{ model: User }],
    });

    const response = await request(app).delete(`/api/biodata/${user.id}`);

    expect(response.statusCode).toBe(200);
    expect(response.body.deletedBiodata.first_name).toEqual(
      userBiodata.first_name
    );
    expect(response.body.deletedBiodata.last_name).toEqual(
      userBiodata.last_name
    );
    expect(response.body.deletedBiodata.country).toEqual(userBiodata.country);
    expect(response.body.deletedBiodata.email).toEqual(userBiodata.email);

    await User.destroy({ where: { username: username } });
  });

  test("422 Validation Error", async () => {
    const response = await request(app).delete(`/api/biodata/test`);

    expect(response.statusCode).toBe(422);
  });

  test("404 Biodata Not Found", async () => {
    const response = await request(app).delete(`/api/biodata/0`);

    expect(response.statusCode).toBe(404);
    expect(response.body.message).toBe("User biodata not found");
  });
});
