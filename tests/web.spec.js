const UserHistory = require("../models").user_game_histories;
const UserBiodata = require("../models").user_game_biodatas;
const User = require("../models").user_game;

const axios = require("axios").default;

const mockRequest = (body = {}, params = {}) => ({ body, params });
const mockResponse = () => {
  const res = {};
  res.render = jest.fn().mockReturnValue(res);
  res.redirect = jest.fn().mockReturnValue(res);
  return res;
};
const webController = require("../controllers/web");

jest.mock("axios");

describe("Get Home View", () => {
  test("200 Success", async () => {
    const req = mockRequest();
    const res = mockResponse();

    webController.getHome(req, res);

    const games = await UserHistory.findAll({
      include: [{ model: User }],
    });

    expect(res.render).toBeCalledWith("index", {
      games: games,
    });
  });
});

describe("Get Users View", () => {
  test("200 Success", async () => {
    const req = mockRequest();
    const res = mockResponse();

    const users = await User.findAll({
      include: [{ model: UserBiodata }],
    });

    const usersData = {
      data: {
        users: users,
      },
    };

    axios.get.mockResolvedValueOnce(usersData);

    await webController.getUsers(req, res);

    expect(res.render).toBeCalledWith("users", usersData.data);
  });
});

describe("Get Edit Game", () => {
  test("200 Success", async () => {
    const req = mockRequest({}, { id: 1 });
    const res = mockResponse();

    const game = await UserHistory.findByPk(1, {
      include: [{ model: User }],
    });

    await webController.getEditGame(req, res);

    expect(res.render).toBeCalledWith("edit-game", {
      game: game,
    });
  });

  test("422 Validation Error", async () => {
    const req = mockRequest({}, {});
    const res = mockResponse();

    await webController.getEditGame(req, res);

    expect(res.render).toBeCalledWith("error", {
      data: new Error("Valid Game ID param is required"),
    });
  });

  test("404 Game Not Found", async () => {
    const req = mockRequest({}, { id: -1 });
    const res = mockResponse();

    await webController.getEditGame(req, res);

    expect(res.render).toBeCalledWith("error", {
      data: new Error("Game Not Found"),
    });
  });
});

describe("Post Edit Game", () => {
  test("200 Success", async () => {
    const updatedGameInfo = {
      title: "Updated Game",
      publisher: "Updated Publisher",
      score: 100,
      hours_time: 50,
    };

    const req = mockRequest(updatedGameInfo, { id: 1 });
    const res = mockResponse();

    await webController.postEditGame(req, res);

    expect(res.redirect).toBeCalledWith("/web");
  });

  test("422 Validation Error", async () => {
    const updatedGameInfo = {
      title: "Updated Game",
      publisher: "Updated Publisher",
      score: 100,
      hours_time: 50,
    };

    const req = mockRequest(updatedGameInfo, {});
    const res = mockResponse();

    await webController.postEditGame(req, res);

    expect(res.render).toBeCalledWith("error", {
      data: new Error("Valid Game ID param is required"),
    });
  });
});

describe("Delete Game", () => {
  test("200 Success", async () => {
    const req = mockRequest({}, { id: 1 });
    const res = mockResponse();

    await webController.deleteGame(req, res);

    expect(res.redirect).toBeCalledWith("/web");
  });

  test("500 System Error", async () => {
    const req = mockRequest({}, { id: 1 });
    const res = mockResponse();

    axios.delete.mockImplementationOnce(() => Promise.reject("error"));

    await webController.deleteGame(req, res);

    expect(res.render).toBeCalledTimes(1);
  });
});

describe("Get Add Game", () => {
  test("200 Success", async () => {
    const req = mockRequest({}, {});
    const res = mockResponse();

    webController.getAddGame(req, res);

    expect(res.render).toBeCalledWith("add-game");
  });
});

describe("Post Add Game", () => {
  test("200 Success", async () => {
    const newGameInfo = {
      username: "budi",
      title: "New Game",
      publisher: "New Publisher",
      score: 100,
      hours_time: 50,
    };

    const req = mockRequest(newGameInfo, {});
    const res = mockResponse();

    await webController.postAddGame(req, res);

    expect(res.redirect).toBeCalledWith("/web");
  });

  test("404 User Not Found", async () => {
    const newGameInfo = {
      username: "unknown0",
      title: "New Game",
      publisher: "New Publisher",
      score: 100,
      hours_time: 50,
    };

    const req = mockRequest(newGameInfo, {});
    const res = mockResponse();

    await webController.postAddGame(req, res);

    expect(res.render).toBeCalledWith("error", {
      data: new Error("User Not Found"),
    });
  });
});

describe("Get Register View", () => {
  test("200 Success", async () => {
    const req = mockRequest({}, {});
    const res = mockResponse();

    webController.getRegister(req, res);

    expect(res.render).toBeCalledWith("register");
  });
});

describe("Post Register", () => {
  test("200 Success", async () => {
    const newUserInfo = {
      username: "NewUser",
      password: "123456",
      firstName: "FirstName",
      lastName: "LastName",
      country: "Country",
      email: "newuser@gmail.com",
    };

    const req = mockRequest(newUserInfo, {});
    const res = mockResponse();

    axios.post.mockResolvedValueOnce({
      data: {
        user: {
          id: 1,
        },
      },
    });

    await webController.postRegister(req, res);

    expect(res.redirect).toBeCalledWith("/web/users");
  });
});

describe("Get Login View", () => {
  test("200 Success", async () => {
    const req = mockRequest({}, {});
    const res = mockResponse();

    webController.getLogin(req, res);

    expect(res.render).toBeCalledWith("login");
  });
});

describe("Post Login", () => {
  test("200 Success", async () => {
    const userInfo = {
      username: "budi",
      password: "123456",
    };

    const req = mockRequest(userInfo, {});
    const res = mockResponse();

    axios.post.mockResolvedValueOnce({
      status: 200,
    });

    await webController.postLogin(req, res);

    expect(res.redirect).toBeCalledWith("/web");
  });

  test("401 Wrong Username / Password", async () => {
    const userInfo = {
      username: "budi",
      password: "wrong",
    };

    const req = mockRequest(userInfo, {});
    const res = mockResponse();

    axios.post.mockResolvedValueOnce({
      status: 400,
      data: {
        message: "Wrong Username / Password",
      },
    });

    await webController.postLogin(req, res);

    expect(res.render).toBeCalledWith("error", {
      data: new Error("Wrong Username / Password"),
    });
  });
});

describe("Get Edit User", () => {
  test("200 Success", async () => {
    const req = mockRequest({}, { userId: 1 });
    const res = mockResponse();

    const biodata = await UserBiodata.findOne({
      where: { user_id: 1 },
      include: [{ model: User }],
    });

    axios.get.mockResolvedValueOnce({
      data: {
        biodata: biodata,
      },
    });

    await webController.getEditUser(req, res);

    expect(res.render).toBeCalledWith("edit-user", {
      userData: biodata,
    });
  });
});

describe("Post Edit User", () => {
  test("200 Success", async () => {
    const updatedUserInfo = {
      firstName: "UpdatedFirstName",
      lastName: "UpdatedLastName",
      country: "UpdatedCountry",
      email: "updatedemail@gmail.com",
    };

    const req = mockRequest(updatedUserInfo, { userId: 1 });
    const res = mockResponse();

    await webController.postEditUser(req, res);

    expect(res.redirect).toBeCalledWith("/web/users");
  });

  test("500 System Error", async () => {
    const updatedUserInfo = {
      firstName: "UpdatedFirstName",
      lastName: "UpdatedLastName",
      country: "UpdatedCountry",
      email: "updatedemail@gmail.com",
    };

    const req = mockRequest(updatedUserInfo, { userId: 1 });
    const res = mockResponse();

    axios.patch.mockImplementationOnce(() => Promise.reject());

    await webController.postEditUser(req, res);

    expect(res.render).toBeCalledTimes(1);
  });
});

describe("Post Delete User", () => {
  test("200 Success", async () => {
    const req = mockRequest({}, { userId: 1 });
    const res = mockResponse();

    await webController.postDeleteUserBio(req, res);

    expect(res.redirect).toBeCalledWith("/web/users");
  });

  test("500 System Error", async () => {
    const req = mockRequest({}, { userId: "test" });
    const res = mockResponse();

    axios.delete.mockImplementationOnce(() => Promise.reject());

    await webController.postDeleteUserBio(req, res);

    expect(res.render).toBeCalledTimes(1);
  });
});
