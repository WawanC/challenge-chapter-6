const UserHistory = require("../models").user_game_histories;
const User = require("../models").user_game;
const axios = require("axios").default;

const apiUrl =
  `http://${process.env.WEB_NAME}/api` || "http://localhost:8000/api";

module.exports = {
  getHome: async (req, res) => {
    const games = await UserHistory.findAll({
      include: [{ model: User }],
    });

    res.render("index", {
      games: games,
    });
  },
  getUsers: async (req, res) => {
    const users = await axios.get(`${apiUrl}/user`);

    res.render("users", {
      users: users.data.users,
    });
  },

  getEditGame: async (req, res) => {
    try {
      const gameId = req.params.id;

      if (!gameId) {
        throw new Error("Valid Game ID param is required");
      }

      const game = await UserHistory.findByPk(gameId, {
        include: [{ model: User }],
      });

      if (!game) {
        throw new Error("Game Not Found");
      }

      res.render("edit-game", {
        game: game,
      });
    } catch (error) {
      res.render("error", {
        data: error,
      });
    }
  },
  postEditGame: async (req, res) => {
    try {
      const gameId = req.params.id;

      if (!gameId) {
        throw new Error("Valid Game ID param is required");
      }

      await axios.patch(
        `${apiUrl}/game/${gameId}`,
        {
          title: req.body.title,
          score: req.body.score,
          hours_time: req.body.hours_time,
          publisher: req.body.publisher,
        },
        {
          headers: {
            "Content-Type": "application/json",
          },
        }
      );

      res.redirect("/web");
    } catch (error) {
      res.render("error", {
        data: error,
      });
    }
  },

  deleteGame: async (req, res) => {
    const gameId = req.params.id;

    try {
      await axios.delete(`${apiUrl}/game/${gameId}`);
      res.redirect("/web");
    } catch (error) {
      res.render("error", {
        data: error,
      });
    }
  },

  getAddGame: (req, res) => {
    res.render("add-game");
  },
  postAddGame: async (req, res) => {
    try {
      const user = await User.findOne({
        where: { username: req.body.username.trim() },
      });

      if (!user) {
        throw new Error("User Not Found");
      }

      await axios.post(
        `${apiUrl}/game/${user.id}`,
        {
          title: req.body.title.trim(),
          publisher: req.body.publisher.trim(),
          score: +req.body.score,
          hours_time: +req.body.hours_time,
        },
        {
          headers: {
            "Content-Type": "application/json",
          },
        }
      );

      res.redirect("/web");
    } catch (error) {
      res.render("error", {
        data: error,
      });
    }
  },

  getRegister: (req, res) => {
    res.render("register");
  },
  postRegister: async (req, res) => {
    const response = await axios.post(
      `${apiUrl}/user`,
      {
        username: req.body.username,
        password: req.body.password,
      },
      {
        headers: {
          "Content-Type": "application/json",
        },
      }
    );

    // await axios.post(
    //   `${apiUrl}/biodata/${response.data.user.id}`,
    //   {
    //     first_name:
    //       req.body.firstName && req.body.firstName.trim().length > 0
    //         ? req.body.firstName
    //         : null,
    //     last_name:
    //       req.body.lastName && req.body.lastName.trim().length > 0
    //         ? req.body.lastName
    //         : null,
    //     email:
    //       req.body.email && req.body.email.trim().length > 0
    //         ? req.body.email
    //         : null,
    //     country:
    //       req.body.country && req.body.country.trim().length > 0
    //         ? req.body.country
    //         : null,
    //   },
    //   {
    //     headers: {
    //       "Content-Type": "application/json",
    //     },
    //   }
    // );

    await axios.post(
      `${apiUrl}/biodata/${response.data.user.id}`,
      {
        first_name: req.body.firstName,
        last_name: req.body.lastName,
        email: req.body.email,
        country: req.body.country,
      },
      {
        headers: {
          "Content-Type": "application/json",
        },
      }
    );

    res.redirect("/web/users");
  },

  getLogin: (req, res) => {
    res.render("login");
  },
  postLogin: async (req, res) => {
    try {
      const response = await axios.post(
        `${apiUrl}/user/login`,
        {
          username: req.body.username.trim(),
          password: req.body.password.trim(),
        },
        {
          headers: {
            "Content-Type": "application/json",
          },
        }
      );

      if (response.status !== 200) {
        throw new Error(response.data.message);
      }

      res.redirect("/web");
    } catch (error) {
      res.render("error", {
        data: error,
      });
    }
  },

  getEditUser: async (req, res) => {
    const userId = req.params.userId;

    const response = await axios.get(`${apiUrl}/biodata/${userId}`);

    res.render("edit-user", {
      userData: response.data.biodata,
    });
  },
  postEditUser: async (req, res) => {
    const userId = req.params.userId;

    try {
      await axios.patch(
        `${apiUrl}/biodata/${userId}`,
        {
          first_name: req.body.firstName.trim(),
          last_name: req.body.lastName.trim(),
          country: req.body.country.trim(),
          email: req.body.email.trim(),
        },
        {
          headers: {
            "Content-Type": "application/json",
          },
        }
      );

      await axios.patch(
        `${apiUrl}/user/${userId}`,
        {
          username: req.body.username,
          password: req.body.password,
        },
        {
          headers: {
            "Content-Type": "application/json",
          },
        }
      );

      res.redirect("/web/users");
    } catch (error) {
      res.render("error", {
        data: error,
      });
    }
  },
  postDeleteUserBio: async (req, res) => {
    const userId = req.params.userId;

    try {
      await axios.delete(`${apiUrl}/user/${userId}`);
      res.redirect("/web/users");
    } catch (error) {
      res.render("error", {
        data: error,
      });
    }
  },
};
