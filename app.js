const express = require("express");
const userRoutes = require("./routes/user");
const biodataRoutes = require("./routes/biodata");
const historyRoutes = require("./routes/history");
const router = require("./routes/web");
const path = require("path");
const swaggerJSON = require("./swagger.json");
const swaggerUI = require("swagger-ui-express");

const app = express();

// Handling CORS
app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, PUT, PATCH, DELETE"
  );
  res.setHeader("Access-Control-Allow-Headers", "Content-Type, Authorization");
  next();
});

// App Setup

app.set("view engine", "ejs");
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, "public")));

// View Router (MVC)
app.use("/web", router);

// API Router (MCR)
app.use("/api/user", userRoutes);
app.use("/api/biodata", biodataRoutes);
app.use("/api/game", historyRoutes);

// Swagger Middleware
app.use("/docs", swaggerUI.serve, swaggerUI.setup(swaggerJSON));

module.exports = app;
