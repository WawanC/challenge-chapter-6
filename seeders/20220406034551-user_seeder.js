"use strict";

module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     *
     */

    await queryInterface.bulkInsert("user_games", [
      {
        username: "budi",
        password: "123456",
      },
      {
        username: "tono",
        password: "98765",
      },
      {
        username: "anto",
        password: "abcdef",
      },
    ]);

    await queryInterface.bulkInsert("user_game_biodatas", [
      {
        user_id: 1,
        first_name: "Budi",
        last_name: "Setiawan",
        country: "Indonesia",
        email: "budisetiawan@gmail.com",
      },
      {
        user_id: 2,
        first_name: "Tono",
        last_name: "Andriawan",
        country: "Malaysia",
        email: "tonoandriawan@gmail.com",
      },
      {
        user_id: 3,
        first_name: "Anto",
        last_name: "Sumanto",
        country: "Thailand",
        email: "antosumanto@gmail.com",
      },
    ]);

    await queryInterface.bulkInsert("user_game_histories", [
      {
        user_id: 1,
        title: "Grand Theft Auto V",
        publisher: "Rockstar",
        score: 90,
        hours_time: 80,
      },
      {
        user_id: 1,
        title: "Valorant",
        publisher: "Riot",
        score: 80,
        hours_time: 30,
      },
      {
        user_id: 1,
        title: "Dota 2",
        publisher: "Valve",
        score: 85,
        hours_time: 100,
      },
      {
        user_id: 2,
        title: "Minecraft",
        publisher: "Mojang",
        score: 75,
        hours_time: 50,
      },
      {
        user_id: 2,
        title: "Genshin Impact",
        publisher: "Mihoyo",
        score: 80,
        hours_time: 40,
      },
      {
        user_id: 2,
        title: "Mobile Legends",
        publisher: "Moontoon",
        score: 85,
        hours_time: 150,
      },
      {
        user_id: 3,
        title: "Player Unknown Battleground",
        publisher: "Tencent",
        score: 75,
        hours_time: 70,
      },
      {
        user_id: 3,
        title: "Among Us",
        publisher: "InnerSloth",
        score: 65,
        hours_time: 25,
      },
      {
        user_id: 3,
        title: "Age Of Empire",
        publisher: "Microsoft",
        score: 85,
        hours_time: 50,
      },
    ]);
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */

    await queryInterface.bulkDelete("user_games", null, {});
    await queryInterface.bulkDelete("user_game_biodatas", null, {});
    await queryInterface.bulkDelete("user_game_histories", null, {});
  },
};
